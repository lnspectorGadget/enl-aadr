module.exports = {
    module: {
        rules: [
            { test: require.resolve("datatables.net-bs"),  use: "imports-loader?this=>window" }
        ]
    }
};

let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
    .sass('resources/assets/sass/app.scss', 'public/css')
    .sass('resources/assets/sass/home.scss', 'public/css')
    .scripts([
        'node_modules/datatables.net/js/jquery.dataTables.js',
        'node_modules/datatables.net-bs/js/dataTables.bootstrap.js'
    ], 'public/js/datatable.js')
    .styles(['node_modules/datatables.net-bs/css.dataTables.bootstrap.css'], 'public/css/datatable.css');
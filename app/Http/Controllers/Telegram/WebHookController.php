<?php

namespace App\Http\Controllers\Telegram;

use App\Http\Controllers\Controller;
use App\Services\TelegramService;
use Longman\TelegramBot\Exception\TelegramException;

class WebHookController extends Controller
{
    public function processWebHook()
    {
        $result = TelegramService::processWebHook();
        if (true === $result) {
            return response()->json(['message' => 'Ok'])->setStatusCode(200);
        } else {
            return response()->json(['message' => $result])->setStatusCode(500);
        }
    }
}

<?php

namespace App\Http\Controllers;

use App\BankItem;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class DashboardController extends Controller
{
    /**
     * Show the application dashboard.
     * @return \Illuminate\Http\Response
     */
    public function index(): View
    {
        $bankItems = BankItem::whereHas('owner', function ($query) {
            $query->where('players.id', Auth::user()->player->id);
        })
            ->with(['item', 'owner', 'capsule.holder'])
            ->get();

        return view('dashboard', [
            'bankItems' => $bankItems
        ]);
    }
}

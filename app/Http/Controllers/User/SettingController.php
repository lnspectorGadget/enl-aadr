<?php

namespace App\Http\Controllers\User;

use App\Capsule;
use App\Http\Requests\User\Delete;
use App\Http\Requests\User\MassDelete;
use App\Http\Requests\User\Register;
use App\Http\Requests\User\Settings;
use App\Http\Requests\User\Store;
use App\Http\Requests\User\Update;
use App\Player;
use App\RegistrationInvite;
use App\Role;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class SettingController extends Controller
{
    /**
     * @param Capsule $capsule
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(User $user)
    {
        return view('user.settings', [
            'user' => $user,
        ]);
    }

    /**
     * @param Update $request
     * @param User $user
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function update(Settings $request, User $user)
    {
        $user->name = $request->input('user.name');
        $user->allow_mail_notifications = $request->input('user.allow_mail_notifications', false) ? true : false;
        $user->allow_telegram_notifications = $request->input('user.allow_telegram_notifications', false) ? true : false;
        $user->telegram_username = $request->input('user.telegram_username');

        if ($request->input('user.password', null) !== null) {
            $user->password = bcrypt($request->input('user.password'));
            $user->setRememberToken(Str::random(60));
        }

        $user->save();

        $player = $user->player()->first();
        $player->name = $request->input('player.name');
        $player->save();

        return redirect(route('user.settings', ['user' => $user]))->with('status', 'Instellingen opgeslagen.');
    }

    /**
     * @param Delete $request
     * @param Capsule $capsule
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function destroy(Delete $request, User $user)
    {
        $user->delete();
        return redirect(route('users.index'));
    }

    /**
     * @param MassDelete $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function massDestroy(MassDelete $request)
    {
        Capsule::destroy($request->input('ids'));

        return redirect(route('capsules.index'));
    }
}

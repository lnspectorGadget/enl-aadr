<?php

namespace App\Http\Controllers\Admin;

use App\Capsule;
use App\Http\Requests\User\Delete;
use App\Http\Requests\User\MassDelete;
use App\Http\Requests\User\Register;
use App\Http\Requests\User\Store;
use App\Http\Requests\User\Update;
use App\Player;
use App\RegistrationInvite;
use App\Role;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::with(['player'])->get();

        return view('admin.users.index', [
            'users' => $users,
        ]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $roles = Role::orderBy('name', 'desc')->get();

        return view('admin.users.create', [
            'roles' => $roles,
        ]);
    }

    /**
     * @param string $registerToken
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function registerCreate(string $registerToken)
    {
        $tokenExists = RegistrationInvite::where('token', '=', $registerToken)->count();
        if ($tokenExists === 1) {
            return view('auth.register', [
                'registerToken' => $registerToken,
            ]);
        }

        return redirect(route('home'));
    }

    /**
     * @param Store $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function registerStore(Register $request)
    {
        $user = new User();
        $user->name = $request->input('user.name');
        $user->email = $request->input('user.email');
        $user->allow_mail_notifications = $request->input('user.allow_mail_notifications', false) ? true : false;
        $user->allow_telegram_notifications = $request->input('user.allow_telegram_notifications', false) ? true : false;
        $user->telegram_username = $request->input('user.telegram_username');

        if ($request->input('user.password', null) !== null) {
            $user->password = Hash::make($request->input('user.password'));
        } else {
            $user->password = Hash::make(Str::random(8));
        }
        $user->setRememberToken(Str::random(60));

        $user->save();

        $role = Role::where('name', '=', 'player')->first();
        $user->attachRole($role);

        $player = new Player();
        $player->name = $request->input('player.name');
        $player->user()->associate($user);
        $player->save();

        Auth::login($user);

        return redirect(route('home'));
    }

    /**
     * @param Store $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function store(Store $request)
    {
        $user = new User();
        $user->name = $request->input('user.name');
        $user->email = $request->input('user.email');
        $user->allow_mail_notifications = $request->input('user.allow_mail_notifications', false) ? true : false;
        $user->allow_telegram_notifications = $request->input('user.allow_telegram_notifications', false) ? true : false;
        $user->telegram_username = $request->input('user.telegram_username');

        if ($request->input('user.password', null) !== null) {
            $user->password = Hash::make($request->input('user.password'));
        } else {
            $user->password = Hash::make(Str::random(8));
        }
        $user->setRememberToken(Str::random(60));

        $user->save();

        $role = Role::uuid($request->input('user.role_id'));
        $user->attachRole($role);

        $player = new Player();
        $player->name = $request->input('player.name');
        $player->user()->associate($user);
        $player->save();

        return redirect(route('users.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * @param Capsule $capsule
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(User $user)
    {
        $roles = Role::orderBy('name', 'desc')->get();

        return view('admin.users.edit', [
            'roles' => $roles,
            'user' => $user,
        ]);
    }

    /**
     * @param Update $request
     * @param User $user
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function update(Update $request, User $user)
    {
        $user->name = $request->input('user.name');
        $user->email = $request->input('user.email');
        $user->allow_mail_notifications = $request->input('user.allow_mail_notifications', false) ? true : false;
        $user->allow_telegram_notifications = $request->input('user.allow_telegram_notifications', false) ? true : false;
        $user->telegram_username = $request->input('user.telegram_username');

        if ($request->input('user.password', null) !== null) {
            $user->password = bcrypt($request->input('user.password'));
            $user->setRememberToken(Str::random(60));
        }

        $user->save();

        $role = Role::uuid($request->input('user.role_id'));
        $user->detachRoles();
        $user->attachRole($role);

        $player = $user->player()->first();
        $player->name = $request->input('player.name');
        $player->save();

        return redirect(route('users.index'));
    }

    /**
     * @param Delete $request
     * @param Capsule $capsule
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function destroy(Delete $request, User $user)
    {
        $user->delete();
        return redirect(route('users.index'));
    }

    /**
     * @param MassDelete $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function massDestroy(MassDelete $request)
    {
        User::destroy($request->input('ids'));

        return redirect(route('users.index'));
    }
}

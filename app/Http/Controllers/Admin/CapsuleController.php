<?php

namespace App\Http\Controllers\Admin;

use App\Capsule;
use App\Http\Requests\Capsule\Delete;
use App\Http\Requests\Capsule\Duplicate;
use App\Http\Requests\Capsule\MassDelete;
use App\Http\Requests\Capsule\Store;
use App\Http\Requests\Capsule\Update;
use App\Item;
use App\Player;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class CapsuleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::user()->hasRole('administrator')) {
            $capsules = Capsule::with(['owner', 'holder'])->get();
        } else {
            $capsules = Capsule::with(['owner', 'holder'])
                ->where('capsules.holder_player_id', '=', Auth::user()->player->id)
                ->get();
        }

        return view('admin.capsules.index', [
            'capsules' => $capsules,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $players = Player::all();

        return view('admin.capsules.create', [
           'players' => $players,
        ]);
    }

    /**
     * @param Store $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function store(Store $request)
    {


        $owner = Player::uuid($request->input('player_owner_id'));
        $holder = Player::uuid($request->input('player_holder_id', Auth::user()->player->id));

        $capsule = new Capsule();
        $capsule->code = $request->input('code');
        $capsule->owner()->associate($owner);
        $capsule->holder()->associate($holder);
        $capsule->save();

        return redirect(route('capsules.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * @param Capsule $capsule
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Capsule $capsule)
    {
        $players = Player::all();

        return view('admin.capsules.edit', [
            'capsule' => $capsule,
            'players' => $players,
        ]);
    }

    /**
     * @param Update $request
     * @param Capsule $capsule
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Update $request, Capsule $capsule)
    {
        $owner = Player::uuid($request->input('player_owner_id'));
        $holder = Player::uuid($request->input('player_holder_id', Auth::user()->player->id));

        $capsule->code = $request->input('code');
        $capsule->owner()->associate($owner);
        $capsule->holder()->associate($holder);
        $capsule->save();

        return redirect(route('capsules.index'));
    }

    /**
     * @param Delete $request
     * @param Capsule $capsule
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function destroy(Delete $request, Capsule $capsule)
    {
        $capsule->delete();
        return redirect(route('capsules.index'));
    }

    /**
     * @param MassDelete $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function massDestroy(MassDelete $request)
    {
        Capsule::destroy($request->input('ids'));

        return redirect(route('capsules.index'));
    }

    /**
     * @param Capsule $capsule
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function createDuplication(Capsule $capsule)
    {
        $items = Item::all();

        return view('admin.capsules.create_duplication', [
            'items' => $items,
            'capsule' => $capsule,
        ]);
    }

    /**
     * @param Duplicate $request
     * @param Capsule $capsule
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function storeDuplication(Duplicate $request, Capsule $capsule)
    {
        $item = Item::uuid($request->input('item_id'));
        $amount = $request->input('amount');

        $capsule->registerDuplication($item, $amount);

        return redirect(route('bankitems.index'));
    }
}

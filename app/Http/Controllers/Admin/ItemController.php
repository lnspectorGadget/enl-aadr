<?php

namespace App\Http\Controllers\Admin;

use App\Item;
use App\Http\Requests\Item\Delete;
use App\Http\Requests\Item\MassDelete;
use App\Http\Requests\Item\Store;
use App\Http\Requests\Item\Update;
use App\Http\Controllers\Controller;

class ItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = Item::all();

        return view('admin.items.index', [
            'items' => $items,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.items.create', [
            'rarities' => Item::RARITIES,
        ]);
    }

    /**
     * @param Store $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function store(Store $request)
    {
        $item = new Item();
        $item->name = $request->input('name');
        $item->rarity = $request->input('rarity');
        $item->save();

        return redirect(route('items.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * @param Item $item
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Item $item)
    {
        return view('admin.items.edit', [
            'item' => $item,
            'rarities' => Item::RARITIES,
        ]);
    }

    /**
     * @param Update $request
     * @param Capsule $capsule
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Update $request, Item $item)
    {
        $item->name = $request->input('name');
        $item->rarity = $request->input('rarity');
        $item->save();

        return redirect(route('items.index'));
    }

    /**
     * @param Delete $request
     * @param Capsule $capsule
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function destroy(Delete $request, Capsule $capsule)
    {
        $capsule->delete();
        return redirect(route('items.index'));
    }

    /**
     * @param MassDelete $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function massDestroy(MassDelete $request)
    {
        Item::destroy($request->input('ids'));

        return redirect(route('items.index'));
    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\BankItem;
use App\Capsule;
use App\Http\Controllers\Controller;
use App\Http\Requests\BankItem\Delete;
use App\Http\Requests\BankItem\MassDelete;
use App\Http\Requests\BankItem\Store;
use App\Http\Requests\BankItem\Update;
use App\Item;
use App\Notifications\BankItem as BankItemNotification;
use App\Player;
use App\Services\TelegramService;
use Illuminate\Support\Facades\Auth;

class BankItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::user()->hasRole('administrator')) {
            $capsules = Capsule::with(['holder', 'items.item', 'items.owner'])
                ->get();
        } else {
            $capsules = Capsule::with(['holder', 'items.item', 'items.owner'])
                ->where('capsules.holder_player_id' ,'=', Auth::user()->player->id)
                ->get();
        }

        return view('admin.bankitems.index', [
            'capsules' => $capsules,
        ]);
    }

    /**
     * @param Capsule $capsule
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create(Capsule $capsule)
    {
        $players = Player::all();
        $items = Item::all();

        return view('admin.bankitems.create', [
            'players' => $players,
            'items' => $items,
            'capsule' => $capsule,
        ]);
    }

    /**
     * @param Store $request
     * @param Capsule $capsule
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function store(Store $request, Capsule $capsule)
    {
        $bankItem = new BankItem();
        $bankItem->capsule()->associate($capsule);

        $item = Item::uuid($request->input('item_id'));
        $player = Player::uuid($request->input('owner_player_id'));

        $bankItem->item()->associate($item);
        $bankItem->owner()->associate($player);
        $bankItem->amount = $request->input('amount');
        $bankItem->save();

        $text = 'A new balance in Capsule: ' . $bankItem->capsule->code . ' has been created with ' . $bankItem->amount . ' ' . $bankItem->item->name . '.';
        if ($bankItem->owner->user->allow_telegram_notifications && $bankItem->owner->user->telegram_chat_id !== null) {
            TelegramService::sendMessage($bankItem->owner->user->telegram_chat_id, $text);
        }
        if ($bankItem->owner->user->allow_mail_notifications) {
            $bankItem->owner->user->notify(new BankItemNotification('ENL-AADR Ingrees Bank - Item Creation', $text));
        }

        return redirect(route('bankitems.index'));
    }

    /**
     * @param BankItem $bankItem
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(BankItem $bankItem)
    {
        $players = Player::all();
        $items = Item::all();

        return view('admin.bankitems.edit', [
            'players' => $players,
            'items' => $items,
            'bankItem' => $bankItem,
        ]);
    }

    /**
     * @param Update $request
     * @param BankItem $bankItem
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Update $request, BankItem $bankItem)
    {
        $item = Item::uuid($request->input('item_id'));
        $player = Player::uuid($request->input('owner_player_id'));

        $oldBankItem = clone $bankItem;

        $bankItem->item()->associate($item);
        $bankItem->owner()->associate($player);
        $bankItem->amount = $request->input('amount');
        $bankItem->save();

        $text = 'Your balance in Capsule: ' . $bankItem->capsule->code . ' has been updated.'."\n".
        'Old value: ' . $oldBankItem->amount . ' ' . $oldBankItem->item->name . '.'."\n".
        'New value: ' . $bankItem->amount . ' ' . $bankItem->item->name . '.';

        if ($bankItem->owner->user->allow_telegram_notifications && $bankItem->owner->user->telegram_chat_id !== null) {
            TelegramService::sendMessage($bankItem->owner->user->telegram_chat_id, $text);
        }
        if ($bankItem->owner->user->allow_mail_notifications) {
            $bankItem->owner->user->notify(new BankItemNotification('ENL-AADR Ingrees Bank - Item Update', $text));
        }

        return redirect(route('bankitems.index'));
    }

    /**
     * @param Delete $request
     * @param BankItem $bankItem
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function destroy(Delete $request, BankItem $bankItem)
    {
        $bankItem->delete();
        return redirect(route('bankitems.index'));
    }

    /**
     * @param MassDelete $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function massDestroy(MassDelete $request)
    {
        BankItem::destroy($request->input('ids'));

        return redirect(route('bankitems.index'));
    }
}

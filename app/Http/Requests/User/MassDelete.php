<?php

namespace App\Http\Requests\User;

use App\User;
use Illuminate\Foundation\Http\FormRequest;

class MassDelete extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $ids = $this->post('ids');
        $users = User::whereIn('id', $ids)->get();

        $canDelete = false;
        foreach ($users as $user) {
            if (!$this->user()->can('delete', $user)) {
                $canDelete = false;
            } else {
                $canDelete = true;
            }
        }

        return $canDelete;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'ids' => 'required|array|exists:users,id',
        ];
    }
}

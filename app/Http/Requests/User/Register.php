<?php

namespace App\Http\Requests\User;

use App\User;
use Illuminate\Foundation\Http\FormRequest;

class Register extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user.name' => 'required|min:6|string',
            'user.email' => 'required|email|unique:users,email',
            'user.password' => 'required|min:6|confirmed',
            'user.allow_mail_notifications' => 'boolean',
            'user.allow_telegram_notifications' => 'boolean',
            'user.telegram_username' => 'required_if:user.allow_telegram_notifications,1',
            'player.name' => 'required|string',
        ];
    }
}

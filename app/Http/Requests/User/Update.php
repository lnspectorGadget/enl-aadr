<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;

class Update extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $user = $this->route('user');
        return $user && $this->user()->can('update', $user);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user.name' => 'required|string',
            'user.email' => 'required|email',
            'user.role_id' => 'required|exists:roles,id',
            'user.password' => 'nullable|min:6|confirmed',
            'user.allow_mail_notifications' => 'boolean',
            'user.allow_telegram_notifications' => 'boolean',
            'user.telegram_username' => 'required_if:user.allow_telegram_notifications,1',
            'player.name' => 'required|string',
        ];
    }
}

<?php

namespace App\Http\Requests\BankItem;

use App\BankItem;
use Illuminate\Foundation\Http\FormRequest;

class Update extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $bankItem = $this->route('bankItem');
        return $this->user()->can('update', $bankItem);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'item_id' => 'required|exists:items,id',
            'owner_player_id' => 'required|exists:players,id',
            'amount' => 'required|min:1',
        ];
    }
}

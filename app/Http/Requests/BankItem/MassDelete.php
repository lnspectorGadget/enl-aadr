<?php

namespace App\Http\Requests\BankItem;

use App\BankItem;
use Illuminate\Foundation\Http\FormRequest;

class MassDelete extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $ids = $this->post('ids');
        $bsnkItems = BankItem::whereIn('id', $ids)->get();

        $canDelete = false;
        foreach ($bsnkItems as $bsnkItem) {
            if (!$this->user()->can('delete', $bsnkItem)) {
                $canDelete = false;
            } else {
                $canDelete = true;
            }
        }

        return $canDelete;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'ids' => 'required|array|exists:capsule_item,id',
        ];
    }
}

<?php

namespace App\Http\Requests\Item;

use App\Item;
use Illuminate\Foundation\Http\FormRequest;

class MassDelete extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $ids = $this->post('ids');
        $items = Item::whereIn('id', $ids)->get();

        $canDelete = false;
        foreach ($items as $item) {
            if (!$this->user()->can('delete', $item)) {
                $canDelete = false;
            } else {
                $canDelete = true;
            }
        }

        return $canDelete;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'ids' => 'required|array|exists:items,id',
        ];
    }
}

<?php

namespace App\Http\Requests\Capsule;

use App\Capsule;
use Illuminate\Foundation\Http\FormRequest;

class Duplicate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $capsule = $this->route('capsule');
        return $this->user()->can('update', $capsule);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'item_id' => 'required|exists:items,id',
            'amount' => 'required|int|min:1',
        ];
    }
}

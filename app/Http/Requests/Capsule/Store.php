<?php

namespace App\Http\Requests\Capsule;

use App\Capsule;
use Illuminate\Foundation\Http\FormRequest;

class Store extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->can('create', Capsule::class);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'code' => 'required|min:8|max:8',
            'player_owner_id' => 'required|exists:players,id',
            'player_holder_id' => 'nullable|exists:players,id',
        ];
    }
}

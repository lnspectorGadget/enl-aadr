<?php

namespace App\Http\Requests\Capsule;

use App\Capsule;
use Illuminate\Foundation\Http\FormRequest;

class MassDelete extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $ids = $this->post('ids');
        $capsules = Capsule::whereIn('id', $ids)->get();

        $canDelete = false;
        foreach ($capsules as $capsule) {
            if (!$this->user()->can('delete', $capsule)) {
                $canDelete = false;
            } else {
                $canDelete = true;
            }
        }

        return $canDelete;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'ids' => 'required|array|exists:capsules,id',
        ];
    }
}

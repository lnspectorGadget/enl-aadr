<?php

namespace App\Http\Requests\Capsule;

use App\Capsule;
use Illuminate\Foundation\Http\FormRequest;

class Update extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $capsule = $this->route('capsule');
        return $capsule && $this->user()->can('update', $capsule);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'code' => 'required|min:8|max:8',
            'player_owner_id' => 'required|exists:players,id',
            'player_holder_id' => 'nullable|exists:players,id',
        ];
    }
}

<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    /**
     * @param User $user
     * @return bool
     */
    public function view(User $user): bool
    {
        if ($user->hasPermission('view_users')) {
            return true;
        }

        return false;
    }

    /**
     * @param User $user
     * @return bool
     */
    public function create(User $user): bool
    {
        if ($user->hasPermission('create_users')) {
            return true;
        }

        return false;
    }

    /**
     * @param User $user
     * @param User $user2
     * @return bool
     */
    public function update(User $user, User $user2): bool
    {
        if ($user->hasPermission('update_users')) {
            if ($user->hasRole('administrator')) {
                return true;
            }

            return $user->id === $user2->id;
        }

        return false;
    }


    /**
     * @param User $user
     * @param User $user2
     * @return bool
     */
    public function delete(User $user, User $user2): bool
    {
        if ($user->hasPermission('delete_users')) {
            return true;
        }

        return false;
    }
}

<?php

namespace App\Policies;

use App\Capsule;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Database\Eloquent\Collection;

class CapsulePolicy
{
    use HandlesAuthorization;

    /**
     * @param User $user
     * @return bool
     */
    public function view(User $user): bool
    {
        if ($user->hasPermission('view_capsules')) {
            return true;
        }

        return false;
    }

    /**
     * @param User $user
     * @return bool
     */
    public function create(User $user): bool
    {
        if ($user->hasPermission('create_capsules')) {
            return true;
        }

        return false;
    }

    /**
     * @param User $user
     * @param Capsule $capsule
     * @return bool
     */
    public function update(User $user, Capsule $capsule): bool
    {
        if ($user->hasPermission('update_capsules')) {
            if ($user->hasRole('administrator')) {
                return true;
            }

            return $user->player->id === $capsule->holder_player_id;
        }

        return false;
    }

    /**
     * @param User $user
     * @param Capsule $capsule
     * @return bool
     */
    public function delete(User $user, Capsule $capsule): bool
    {
        if ($user->hasPermission('delete_capsules')) {
            if ($user->hasRole('administrator')) {
                return true;
            }

            return $user->player->id === $capsule->holder_player_id;
        }

        return false;
    }
}

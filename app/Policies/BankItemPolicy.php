<?php

namespace App\Policies;

use App\BankItem;
use App\Capsule;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class BankItemPolicy
{
    use HandlesAuthorization;

    /**
     * @param User $user
     * @return bool
     */
    public function view(User $user): bool
    {
        if ($user->hasPermission('view_bankitems')) {
            return true;
        }

        return false;
    }

    /**
     * @param User $user
     * @return bool
     */
    public function create(User $user): bool
    {
        if ($user->hasPermission('create_bankitems')) {
            return true;
        }

        return false;
    }

    /**
     * @param User $user
     * @param BankItem $bankItem
     * @return bool
     */
    public function update(User $user, BankItem $bankItem): bool
    {
        if ($user->hasPermission('update_bankitems')) {
            if ($user->hasRole('administrator')) {
                return true;
            }

            return $user->player->id === $bankItem->capsule->holder_player_id;
        }

        return false;
    }


    /**
     * @param User $user
     * @param BankItem $bankItem
     * @return bool
     */
    public function delete(User $user, BankItem $bankItem): bool
    {
        if ($user->hasPermission('delete_capsules')) {
            if ($user->hasRole('administrator')) {
                return true;
            }

            return $user->player->id === $bankItem->capsule->holder_player_id;
        }

        return false;
    }

    /**
     * @param User $user
     * @return bool
     */
    public function massDelete(User $user): bool
    {
        if ($user->hasRole('administrator')) {
            return true;
        }

        return false;
    }
}

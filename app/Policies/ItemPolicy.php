<?php

namespace App\Policies;

use App\Item;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ItemPolicy
{
    use HandlesAuthorization;

    /**
     * @param User $user
     * @return bool
     */
    public function view(User $user): bool
    {
        if ($user->hasPermission('view_items')) {
            return true;
        }

        return false;
    }

    /**
     * @param User $user
     * @return bool
     */
    public function create(User $user): bool
    {
        if ($user->hasPermission('create_items')) {
            return true;
        }

        return false;
    }

    /**
     * @param User $user
     * @param Item $item
     * @return bool
     */
    public function update(User $user, Item $item): bool
    {
        if ($user->hasPermission('update_items')) {
            return true;
        }

        return false;
    }


    /**
     * @param User $user
     * @param Item $item
     * @return bool
     */
    public function delete(User $user, Item $item): bool
    {
        if ($user->hasPermission('delete_items')) {
            return true;
        }

        return false;
    }
}

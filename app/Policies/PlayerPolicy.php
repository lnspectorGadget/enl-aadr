<?php

namespace App\Policies;

use App\Player;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class PlayerPolicy
{
    use HandlesAuthorization;

    /**
     * @param User $user
     * @return bool
     */
    public function view(User $user): bool
    {
        if ($user->hasPermission('view_players')) {
            return true;
        }

        return false;
    }

    /**
     * @param User $user
     * @return bool
     */
    public function create(User $user): bool
    {
        if ($user->hasPermission('create_players')) {
            return true;
        }

        return false;
    }

    /**
     * @param User $user
     * @param Player $player
     * @return bool
     */
    public function update(User $user, Player $player): bool
    {
        if ($user->hasPermission('update_players')) {
            return true;
        }

        return $user->player->id === $player->id;
    }


    /**
     * @param User $user
     * @param Player $player
     * @return bool
     */
    public function delete(User $user, Player $player): bool
    {
        if ($user->hasPermission('delete_players')) {
            return true;
        }

        return false;
    }
}

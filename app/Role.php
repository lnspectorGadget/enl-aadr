<?php

namespace App;

use App\Support\Model\UuidModelTrait;
use Zizaco\Entrust\EntrustRole;

class Role extends EntrustRole
{
    use UuidModelTrait;

    public $incrementing = false;
}

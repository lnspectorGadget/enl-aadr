<?php

namespace App;

use App\Support\Model\UuidModelTrait;
use Zizaco\Entrust\EntrustPermission;

class Permission extends EntrustPermission
{
    use UuidModelTrait;

    public $incrementing = false;
}

<?php

namespace App;

use Iatstuti\Database\Support\CascadeSoftDeletes;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Player extends UuidModel
{
    use CascadeSoftDeletes;

    protected $cascadeDeletes = ['items'];

    /**
     * @return HasMany
     */
    public function owningCapsules(): HasMany
    {
        return $this->hasMany(Capsule::class, 'owner_player_id');
    }

    /**
     * @return HasMany
     */
    public function holdingCapsules(): HasMany
    {
        return $this->hasMany(Capsule::class, 'holder_player_id');
    }

    /**
     * @return HasMany
     */
    public function items(): HasMany
    {
        return $this->hasMany(BankItem::class, 'owner_player_id');
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}

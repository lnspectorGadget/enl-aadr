<?php

namespace App;

use App\Notifications\ItemDuplication;
use App\Services\TelegramService;
use GuzzleHttp\Client;
use Iatstuti\Database\Support\CascadeSoftDeletes;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use App\Notifications\BankItem as BankItemNotification;

class Capsule extends UuidModel
{
    use CascadeSoftDeletes;

    protected $cascadeDeletes = ['items'];

    /**
     * @return BelongsTo
     */
    public function owner(): BelongsTo
    {
        return $this->belongsTo(Player::class, 'owner_player_id');
    }

    /**
     * @return BelongsTo
     */
    public function holder(): BelongsTo
    {
        return $this->belongsTo(Player::class, 'holder_player_id');
    }

    /**
     * @return HasMany
     */
    public function items(): HasMany
    {
        return $this->hasMany(BankItem::class, 'capsule_id');
    }

    /**
     * @param Item $item
     * @param int $amount
     * @throws \Exception
     */
    public function registerDuplication(Item $item, int $amount): void
    {
        $bankItems = $this->items()->where('item_id', '=', $item->id);
        $bankItem_count = $bankItems->sum('amount');

        /** @var BankItem $bankItem */
        foreach ($bankItems->get() as $bankItem) {
            $bankAmount = ($amount * ($bankItem->amount / $bankItem_count));
            $bankItem->amount = $bankItem->amount + $bankAmount;
            $bankItem->save();

            $text = 'Your ' . $bankItem->item->name . ' balance in Capsule: ' . $bankItem->capsule->code . ' has been updated to ' . $this->floorDecimal($bankItem->amount, 3) . '.';
            if ($bankItem->owner->user->allow_telegram_notifications && $bankItem->owner->user->telegram_chat_id !== null) {
                TelegramService::sendMessage($bankItem->owner->user->telegram_chat_id, $text);
            }
            if ($bankItem->owner->user->allow_mail_notifications) {
                $bankItem->owner->user->notify(new BankItemNotification('ENL-AADR Ingrees Bank - Item Duplication', $text));
            }
        }
    }

    /**
     * @param $float
     * @param int $decimals
     * @return float
     */
    private function floorDecimal($float, $decimals=2): float
    {

        return floor($float * pow(10, $decimals)) / pow(10, $decimals);
    }
}

<?php

namespace App;

use App\Support\Model\UuidModelTrait;
use Iatstuti\Database\Support\CascadeSoftDeletes;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Support\User\EntrustUserTrait;
use Ramsey\Uuid\Uuid;

class User extends Authenticatable
{
    use Notifiable;
    use EntrustUserTrait {
        attachRole as protected traitAttachRole;
        restore as private restoreEntrustUser;
    }
    use SoftDeletes {
        restore as private restoreSoftDeletes;
    }
    use UuidModelTrait, CascadeSoftDeletes;

    protected $cascadeDeletes = ['player'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $dates = ['deleted_at'];

    public $incrementing = false;

    /**
     * User constructor.
     * @param array $attributes
     * @throws \Exception
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->id = Uuid::uuid4()->toString();
    }

    public function player(): HasOne
    {
        return $this->hasOne(Player::class);
    }

    /**
     * Method for fixing the multiple use of Traits with colliding methods
     */
    public function restore()
    {
        $this->restoreEntrustUser();
        $this->restoreSoftDeletes();
    }
}

<?php

namespace App;

use Iatstuti\Database\Support\CascadeSoftDeletes;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Item extends UuidModel
{
    use CascadeSoftDeletes;

    const RARITIES = [
        'Common',
        'Rare',
        'Very Rare',
    ];

    protected $cascadeDeletes = ['inventory'];

    /**
     * @return string
     */
    public function getRarity(): string
    {
        return self::RARITIES[$this->rarity];
    }

    /**
     * @return HasMany
     */
    public function inventory(): HasMany
    {
        return $this->hasMany(BankItem::class);
    }
}

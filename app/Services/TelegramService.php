<?php

namespace App\Services;

use GuzzleHttp\Client;
use Longman\TelegramBot\Telegram as TelegramBot;
use Longman\TelegramBot\Exception\TelegramException as TelegramBotException;

class TelegramService
{
    /** @var TelegramBot $telegramBot */
    private $telegramBot;

    /**
     * TelegramService constructor.
     * @throws TelegramBotException
     */
    public function __construct()
    {
        try {
            $this->telegramBot = new TelegramBot(config('app.telegram_bot_api_key'), config('app.telegram_bot_username'));
        } catch (TelegramBotException $e) {
            throw $e;
        }
    }

    public static function setup()
    {
        try {
            $telegramService = new self();
            $telegramService->telegramBot->setWebhook(config('app.telegram_bot_webhook_url') !== null ? config('app.telegram_bot_webhook_url') : route('telegram.webhook'));
        } catch (TelegramBotException $e) {
            return $e->getMessage();
        }
    }

    /**
     * @return bool|string
     */
    public static function processWebHook()
    {
        try {
            $telegramService = new self();
            $telegramService->telegramBot->addCommandsPath(__DIR__ . '/../Support/Telegram/Commands/');

            // Handle telegram webhook request
            return $telegramService->telegramBot->handle();
        } catch (TelegramBotException $e) {
            return $e->getMessage();
        }
    }

    /**
     * @param string $chat_id
     * @param string $message
     */
    public static function sendMessage(string $chat_id, string $message)
    {
        $client = new Client();
        $url = 'https://api.telegram.org/bot' . config('app.telegram_bot_api_key') . '/sendMessage?chat_id=' . $chat_id . '&text=' . $message;
        $client->get($url);
    }
}
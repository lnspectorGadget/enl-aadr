<?php

namespace App;

use App\Support\Model\UuidModelTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Ramsey\Uuid\Uuid;

class UuidModel extends Model
{
    use SoftDeletes, UuidModelTrait;

    protected $dates = ['deleted_at'];

    public $incrementing = false;

    /**
     * BankItem constructor.
     * @param array $attributes
     * @throws \Exception
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->id = Uuid::uuid4()->toString();
    }
}

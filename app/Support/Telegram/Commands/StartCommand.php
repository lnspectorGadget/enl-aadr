<?php

namespace Longman\TelegramBot\Commands\UserCommands;

use App\User;
use Longman\TelegramBot\Commands\UserCommand;
use Longman\TelegramBot\Request;

class StartCommand extends UserCommand
{
    /**
     * @var string
     */
    protected $name = 'start';
    /**
     * @var string
     */
    protected $description = 'Connect bot to your account on enl-aadr.nl';
    /**
     * @var string
     */
    protected $usage = '/start';
    /**
     * @var string
     */
    protected $version = '0.0.1';
    /**
     * @var bool
     */
    protected $need_mysql = false;
    /**
     * @var bool
     */
    protected $private_only = true;
    /**
     * Conversation Object
     *
     * @var \Longman\TelegramBot\Conversation
     */
    protected $conversation;
    /**
     * Command execute method
     *
     * @return \Longman\TelegramBot\Entities\ServerResponse
     * @throws \Longman\TelegramBot\Exception\TelegramException
     */
    public function execute()
    {
        $message = $this->getMessage();
        $chat_id = $message->getChat()->getId();
        $sender = '@' . $message->getFrom()->getUsername();

        $user = User::where('telegram_username', '=', $sender)->first();
        if ($user instanceof User) {
            $user->telegram_chat_id = $chat_id;
            $user->save();

            $data = [
                'chat_id' => $chat_id,
                'text'    => 'Ingress Bank Bot has been connected to your account on https://enl-aadr.nl with player IGN: ' . $user->player->name,
                'status'  => 'Success',
            ];

        } else {
            $data = [
                'chat_id' => $chat_id,
                'text'    => 'Ingress Bank Bot could not be connected.' . "\n" . 'Make sure your Telegram username has been set properly in your account on https://enl-aadr.nl',
                'status'  => 'Failed',
            ];
        }

        return Request::sendMessage($data);
    }
}
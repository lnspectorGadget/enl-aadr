<?php

namespace App\Support\Model;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

trait UuidModelTrait
{
    /**
     * Scope a query to only include models matching the supplied UUID.
     * Returns the model by default, or supply a second flag `false` to get the Query Builder instance.
     *
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     *
     * @param  Builder      $query The Query Builder instance.
     * @param  string       $uuid  The UUID of the model.
     * @param  bool|true    $first Returns the model by default, or set to `false` to chain for query builder.
     * @return Model|Builder
     */
    public function scopeUuid(Builder $query, $uuid, $first = true)
    {
        if (!is_string($uuid) || (preg_match('/^[0-9a-f]{8}-[0-9a-f]{4}-4[0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/', $uuid) !== 1)) {
            throw (new ModelNotFoundException())->setModel(get_class($this));
        }

        $search = $query->where('id', $uuid);

        return $first ? $search->firstOrFail() : $search;
    }
}
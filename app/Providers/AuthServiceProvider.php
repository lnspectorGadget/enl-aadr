<?php

namespace App\Providers;

use App\BankItem;
use App\Capsule;
use App\Item;
use App\Policies\BankItemPolicy;
use App\Policies\CapsulePolicy;
use App\Policies\ItemPolicy;
use App\Policies\UserPolicy;
use App\User;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        Capsule::class => CapsulePolicy::class,
        Item::class => ItemPolicy::class,
        BankItem::class => BankItemPolicy::class,
        User::class => UserPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
    }
}

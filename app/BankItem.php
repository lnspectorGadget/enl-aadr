<?php

namespace App;

use Illuminate\Database\Eloquent\Relations\BelongsTo;

class BankItem extends UuidModel
{
    protected $table = 'capsule_item';

    /**
     * @return BelongsTo
     */
    public function capsule(): BelongsTo
    {
        return $this->belongsTo(Capsule::class);
    }

    /**
     * @return BelongsTo
     */
    public function item(): BelongsTo
    {
        return $this->belongsTo(Item::class);
    }

    /**
     * @return BelongsTo
     */
    public function owner(): BelongsTo
    {
        return $this->belongsTo(Player::class, 'owner_player_id');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;
use Ramsey\Uuid\Uuid;

class FillTestData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     * @throws Exception
     */
    public function up()
    {
        $user1_uuid = Uuid::uuid4()->toString();
        $user2_uuid = Uuid::uuid4()->toString();
        $user3_uuid = Uuid::uuid4()->toString();

        $player1_uuid = Uuid::uuid4()->toString();
        $player2_uuid = Uuid::uuid4()->toString();
        $player3_uuid = Uuid::uuid4()->toString();

        $item1_uuid = Uuid::uuid4()->toString();
        $item2_uuid = Uuid::uuid4()->toString();
        $item3_uuid = Uuid::uuid4()->toString();
        $item4_uuid = Uuid::uuid4()->toString();
        $item5_uuid = Uuid::uuid4()->toString();
        $item6_uuid = Uuid::uuid4()->toString();
        $item7_uuid = Uuid::uuid4()->toString();

        $capsule1_uuid = Uuid::uuid4()->toString();
        $capsule2_uuid = Uuid::uuid4()->toString();
        $capsule3_uuid = Uuid::uuid4()->toString();


        DB::table('users')->insert([
            ['id' => $user1_uuid, 'name' => 'Richard Admin', 'email' => 'richard+admin@hoogstraaten.eu', 'password' => bcrypt('secret')],
            ['id' => $user2_uuid, 'name' => 'Richard Banker', 'email' => 'richard+banker@hoogstraaten.eu', 'password' => bcrypt('secret')],
            ['id' => $user3_uuid, 'name' => 'Richard Player', 'email' => 'richard+player@hoogstraaten.eu', 'password' => bcrypt('secret')],
        ]);
        DB::table('players')->insert([
            ['id' => $player1_uuid, 'name' => 'Insp3ctorGadget', 'user_id' => $user1_uuid],
            ['id' => $player2_uuid, 'name' => 'Sideshowback', 'user_id' => $user2_uuid],
            ['id' => $player3_uuid, 'name' => 'Snorski', 'user_id' => $user3_uuid],
        ]);
        DB::table('items')->insert([
            ['id' => $item1_uuid, 'name' => 'Aegis Shield', 'rarity' => 2],
            ['id' => $item2_uuid, 'name' => 'Ada Refactor', 'rarity' => 2],
            ['id' => $item3_uuid, 'name' => 'Jarvis Virus', 'rarity' => 2],
            ['id' => $item4_uuid, 'name' => 'Portal Shield', 'rarity' => 2],
            ['id' => $item5_uuid, 'name' => 'ITO EN Transmuter (-)', 'rarity' => 2],
            ['id' => $item6_uuid, 'name' => 'ITO EN Transmuter (+)', 'rarity' => 2],
            ['id' => $item7_uuid, 'name' => 'Lawson Power Cube', 'rarity' => 2],
        ]);
        DB::table('capsules')->insert([
            ['id' => $capsule1_uuid, 'code' => '11111111', 'owner_player_id' => $player1_uuid, 'holder_player_id' => $player1_uuid],
            ['id' => $capsule2_uuid, 'code' => '22222222', 'owner_player_id' => $player1_uuid, 'holder_player_id' => $player1_uuid],
            ['id' => $capsule3_uuid, 'code' => '33333333', 'owner_player_id' => $player2_uuid, 'holder_player_id' => $player2_uuid],
        ]);
        DB::table('capsule_item')->insert([
            ['id' => Uuid::uuid4()->toString(), 'capsule_id' => $capsule1_uuid,'item_id' => $item1_uuid, 'owner_player_id' => $player1_uuid, 'amount' => 20],
            ['id' => Uuid::uuid4()->toString(), 'capsule_id' => $capsule1_uuid,'item_id' => $item2_uuid, 'owner_player_id' => $player1_uuid, 'amount' => 30],
            ['id' => Uuid::uuid4()->toString(), 'capsule_id' => $capsule2_uuid,'item_id' => $item1_uuid, 'owner_player_id' => $player1_uuid, 'amount' => 20],
            ['id' => Uuid::uuid4()->toString(), 'capsule_id' => $capsule2_uuid,'item_id' => $item3_uuid, 'owner_player_id' => $player2_uuid, 'amount' => 40],
            ['id' => Uuid::uuid4()->toString(), 'capsule_id' => $capsule2_uuid,'item_id' => $item2_uuid, 'owner_player_id' => $player2_uuid, 'amount' => 20],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::statement('TRUNCATE TABLE `capsule_item`;');
        DB::statement('TRUNCATE TABLE `capsules`;');
        DB::statement('TRUNCATE TABLE `items`;');
        DB::statement('TRUNCATE TABLE `players`;');
        DB::statement('TRUNCATE TABLE `users`;');
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}

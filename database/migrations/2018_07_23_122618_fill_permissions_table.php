<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Ramsey\Uuid\Uuid;

class FillPermissionsTable extends Migration
{
    /**
     * Run the migrations
     *
     * @return void
     * @throws Exception
     */
    public function up()
    {
        DB::table('permissions')->insert([
            ['id' => Uuid::uuid4()->toString(), 'name' => 'view_capsules', 'display_name' => 'View Capsules'],
            ['id' => Uuid::uuid4()->toString(), 'name' => 'create_capsules', 'display_name' => 'Create Capsules'],
            ['id' => Uuid::uuid4()->toString(), 'name' => 'update_capsules', 'display_name' => 'Update Capsules'],
            ['id' => Uuid::uuid4()->toString(), 'name' => 'delete_capsules', 'display_name' => 'Delete Capsules'],

            ['id' => Uuid::uuid4()->toString(), 'name' => 'view_items', 'display_name' => 'View Items'],
            ['id' => Uuid::uuid4()->toString(), 'name' => 'create_items', 'display_name' => 'Create Items'],
            ['id' => Uuid::uuid4()->toString(), 'name' => 'update_items', 'display_name' => 'Update Items'],
            ['id' => Uuid::uuid4()->toString(), 'name' => 'delete_items', 'display_name' => 'Delete Items'],

            ['id' => Uuid::uuid4()->toString(), 'name' => 'view_users', 'display_name' => 'View Users'],
            ['id' => Uuid::uuid4()->toString(), 'name' => 'create_users', 'display_name' => 'Create Users'],
            ['id' => Uuid::uuid4()->toString(), 'name' => 'update_users', 'display_name' => 'Update Users'],
            ['id' => Uuid::uuid4()->toString(), 'name' => 'delete_users', 'display_name' => 'Delete Users'],

            ['id' => Uuid::uuid4()->toString(), 'name' => 'view_bankitems', 'display_name' => 'View BankItems'],
            ['id' => Uuid::uuid4()->toString(), 'name' => 'create_bankitems', 'display_name' => 'Create BankItems'],
            ['id' => Uuid::uuid4()->toString(), 'name' => 'update_bankitems', 'display_name' => 'Update BankItems'],
            ['id' => Uuid::uuid4()->toString(), 'name' => 'delete_bankitems', 'display_name' => 'Delete BankItems'],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::statement('TRUNCATE TABLE `permissions`;');
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}

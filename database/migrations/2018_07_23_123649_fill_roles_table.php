<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Ramsey\Uuid\Uuid;

class FillRolesTable extends Migration
{
    /**
     * Run the migrations
     *
     * @return void
     * @throws Exception
     */
    public function up()
    {
        DB::table('roles')->insert([
            ['id' => Uuid::uuid4()->toString(), 'name' => 'administrator', 'display_name' => 'Administrator'],
            ['id' => Uuid::uuid4()->toString(), 'name' => 'banker', 'display_name' => 'Banker'],
            ['id' => Uuid::uuid4()->toString(), 'name' => 'player', 'display_name' => 'Player'],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::statement('TRUNCATE TABLE `roles`;');
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}

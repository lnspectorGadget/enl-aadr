<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;
use App\User;
use App\Role;

class AddUsersToRoles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $user = User::where('email' , '=', 'richard+admin@hoogstraaten.eu')->first();
        $role = Role::where('name', '=', 'administrator')->first();
        $user->attachRole($role);

        $user = User::where('email' , '=', 'richard+banker@hoogstraaten.eu')->first();
        $role = Role::where('name', '=', 'banker')->first();
        $user->attachRole($role);

        $user = User::where('email' , '=', 'richard+player@hoogstraaten.eu')->first();
        $role = Role::where('name', '=', 'player')->first();
        $user->attachRole($role);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::statement('TRUNCATE TABLE `role_user`;');
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}

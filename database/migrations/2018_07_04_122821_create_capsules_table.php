<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCapsulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('capsules', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary(['id']);
            $table->string('code');
            $table->decimal('duplication_rate')->default(0.00);
            $table->uuid('owner_player_id');
            $table->foreign('owner_player_id')->references('id')->on('players');
            $table->uuid('holder_player_id');
            $table->foreign('holder_player_id')->references('id')->on('players');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('capsules');
    }
}

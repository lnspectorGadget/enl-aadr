<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCapsuleItemTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('capsule_item', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary(['id']);
            $table->uuid('capsule_id');
            $table->foreign('capsule_id')->references('id')->on('capsules')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->uuid('item_id');
            $table->foreign('item_id')->references('id')->on('items')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->uuid('owner_player_id');
            $table->foreign('owner_player_id')->references('id')->on('players')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->decimal('amount', 8, 3)->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('capsule_item');
    }
}

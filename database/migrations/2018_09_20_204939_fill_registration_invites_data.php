<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class FillRegistrationInvitesData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     * @throws Exception
     */
    public function up()
    {
        DB::table('registration_invites')->insert([
            ['token' => bin2hex(random_bytes(32))],
            ['token' => bin2hex(random_bytes(32))],
            ['token' => bin2hex(random_bytes(32))],
            ['token' => bin2hex(random_bytes(32))],
            ['token' => bin2hex(random_bytes(32))],
            ['token' => bin2hex(random_bytes(32))],
            ['token' => bin2hex(random_bytes(32))],
            ['token' => bin2hex(random_bytes(32))],
            ['token' => bin2hex(random_bytes(32))],
            ['token' => bin2hex(random_bytes(32))],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::statement('TRUNCATE TABLE `registration_invites`;');
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}

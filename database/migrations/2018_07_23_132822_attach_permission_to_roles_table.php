<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Role;
use App\Permission;

class AttachPermissionToRolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /** @var Role $role */

        $role = Role::where('name', 'administrator')->first();
        $permissions = Permission::all();
        $role->attachPermissions($permissions);

        $role = Role::where('name', 'banker')->first();
        $permissions = Permission::where('name', 'view_capsules')
            ->orWhere('name', 'create_capsules')
            ->orWhere('name', 'update_capsules')
            ->orWhere('name', 'delete_capsules')
            ->orWhere('name', 'view_bankitems')
            ->orWhere('name', 'create_bankitems')
            ->orWhere('name', 'update_bankitems')
            ->orWhere('name', 'delete_bankitems')
            ->get();
        $role->attachPermissions($permissions);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::statement('TRUNCATE TABLE `permission_role`;');
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}

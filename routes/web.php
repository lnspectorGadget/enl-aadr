<?php

Route::get('/', 'StaticController@index')->name('home');

Route::middleware(['auth'])->group(function () {
    Route::get('/dashboard', 'DashboardController@index')->name('dashboard');

    Route::group(['prefix' => 'user', 'namespace' => 'User'],
        function () {
            Route::get('/{user}/settings', 'SettingController@edit')->name('user.settings');
            Route::patch('/{user}/settings', 'SettingController@update')->name('user.settings.update');
        });

    Route::group(['prefix' => 'admin', 'namespace' => 'Admin'],
        function () {
            #BankItems
            Route::group(['prefix' => 'bank-items', 'middleware' => ['role:administrator|banker']],
                function () {
                    Route::get('/', 'BankItemController@index')->name('bankitems.index');
                    Route::get('/create/{capsule}', 'BankItemController@create')->name('bankitems.create');
                    route::get('/edit/{bankItem}', 'BankItemController@edit')->name('bankitems.edit');
                    Route::post('/{capsule}', 'BankItemController@store')->name('bankitems.store');
                    Route::get('/duplication/create', 'BankItemController@createDuplication')->name('bankitems.duplication.create');
                    Route::post('/duplication', 'BankItemController@storeDuplication')->name('bankitems.duplication.store');
                    Route::patch('edit/{bankItem}', 'BankItemController@update')->name('bankitems.update');
                    Route::delete('/', 'BankItemController@massDestroy')->name('bankitems.mass-delete');
                    Route::delete('/{bankItem}', 'BankItemController@destroy')->name('bankitems.delete');
                });

            #Capsules
            Route::group(['prefix' => 'capsules', 'middleware' => ['role:administrator|banker']],
                function () {
                    Route::get('/', 'CapsuleController@index')->name('capsules.index');
                    Route::get('/create', 'CapsuleController@create')->name('capsules.create');
                    route::get('/edit/{capsule}', 'CapsuleController@edit')->name('capsules.edit');
                    Route::get('/{capsule}/create-duplication', 'CapsuleController@createDuplication')->name('capsules.create-duplication');
                    Route::post('/', 'CapsuleController@store')->name('capsules.store');
                    Route::post('/{capsule}/store-duplication', 'CapsuleController@storeDuplication')->name('capsules.store-duplication');
                    Route::patch('edit/{capsule}', 'CapsuleController@update')->name('capsules.update');
                    Route::delete('/', 'CapsuleController@massDestroy')->name('capsules.mass-delete');
                    Route::delete('/{capsule}', 'CapsuleController@destroy')->name('capsules.delete');
                });

            #Items
            Route::group(['prefix' => 'items', 'middleware' => ['role:administrator']],
                function () {
                    Route::get('/', 'ItemController@index')->name('items.index');
                    Route::get('/create', 'ItemController@create')->name('items.create');
                    route::get('/edit/{item}', 'ItemController@edit')->name('items.edit');
                    Route::post('/', 'ItemController@store')->name('items.store');
                    Route::patch('edit/{item}', 'ItemController@update')->name('items.update');
                    Route::delete('/', 'ItemController@massDestroy')->name('items.mass-delete');
                    Route::delete('/{item}', 'ItemController@destroy')->name('items.delete');
                });

            #Players
            Route::group(['prefix' => 'users', 'middleware' => ['role:administrator']],
                function () {
                    Route::get('/', 'UserController@index')->name('users.index');
                    Route::get('/create', 'UserController@create')->name('users.create');
                    route::get('/edit/{user}', 'UserController@edit')->name('users.edit');
                    Route::post('/', 'UserController@store')->name('users.store');
                    Route::patch('edit/{user}', 'UserController@update')->name('users.update');
                    Route::delete('/', 'UserController@massDestroy')->name('users.mass-delete');
                    Route::delete('/{user}', 'UserController@destroy')->name('users.delete');
                });
        });
});

\Illuminate\Support\Facades\Auth::routes();
Route::group(['namespace' => 'Admin'],
    function () {
        Route::get('/register/{registerToken}', 'UserController@registerCreate')->name('users.register-create');
        Route::post('/register/{registerToken}', 'UserController@registerStore')->name('users.register-store');
    });

Route::group(['namespace' => 'Telegram'],
    function () {
        Route::post('/telegram-hook', 'WebHookController@processWebHook')->name('telegram.webhook');
    });

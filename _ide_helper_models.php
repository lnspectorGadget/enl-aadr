<?php

// @formatter:off
/**
 * A helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace App{
/**
 * App\BankItem
 *
 * @property string $id
 * @property string $capsule_id
 * @property string $item_id
 * @property string $owner_player_id
 * @property float $amount
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read \App\Capsule $capsule
 * @property-read \App\Item $item
 * @property-read \App\Player $owner
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BankItem whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BankItem whereCapsuleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BankItem whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BankItem whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BankItem whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BankItem whereItemId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BankItem whereOwnerPlayerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BankItem whereUpdatedAt($value)
 */
	class BankItem extends \Eloquent {}
}

namespace App{
/**
 * App\Capsule
 *
 * @property string $id
 * @property string $code
 * @property float $duplication_rate
 * @property string $owner_player_id
 * @property string $holder_player_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read \App\Player $holder
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\BankItem[] $items
 * @property-read \App\Player $owner
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Capsule whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Capsule whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Capsule whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Capsule whereDuplicationRate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Capsule whereHolderPlayerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Capsule whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Capsule whereOwnerPlayerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Capsule whereUpdatedAt($value)
 */
	class Capsule extends \Eloquent {}
}

namespace App{
/**
 * App\Item
 *
 * @property string $id
 * @property string $name
 * @property int $rarity
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\BankItem[] $inventory
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Item whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Item whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Item whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Item whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Item whereRarity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Item whereUpdatedAt($value)
 */
	class Item extends \Eloquent {}
}

namespace App{
/**
 * App\Permission
 *
 * @property string $id
 * @property string $name
 * @property string|null $display_name
 * @property string|null $description
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Role[] $roles
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Permission uuid($uuid, $first = true)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Permission whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Permission whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Permission whereDisplayName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Permission whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Permission whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Permission whereUpdatedAt($value)
 */
	class Permission extends \Eloquent {}
}

namespace App{
/**
 * App\Player
 *
 * @property string $id
 * @property string $user_id
 * @property string $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Capsule[] $holdingCapsules
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\BankItem[] $items
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Capsule[] $owningCapsules
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Player whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Player whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Player whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Player whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Player whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Player whereUserId($value)
 */
	class Player extends \Eloquent {}
}

namespace App{
/**
 * App\RegistrationInvite
 *
 * @property string $token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\RegistrationInvite whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\RegistrationInvite whereToken($value)
 */
	class RegistrationInvite extends \Eloquent {}
}

namespace App{
/**
 * App\Role
 *
 * @property string $id
 * @property string $name
 * @property string|null $display_name
 * @property string|null $description
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Permission[] $perms
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\User[] $users
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Role uuid($uuid, $first = true)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Role whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Role whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Role whereDisplayName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Role whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Role whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Role whereUpdatedAt($value)
 */
	class Role extends \Eloquent {}
}

namespace App{
/**
 * App\User
 *
 * @property string $id
 * @property string $name
 * @property string $email
 * @property string $password
 * @property string|null $telegram_username
 * @property string|null $telegram_chat_id
 * @property string|null $remember_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property int $allow_mail_notifications
 * @property int $allow_telegram_notifications
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read \App\Player $player
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Role[] $roles
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\User onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User uuid($uuid, $first = true)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereAllowMailNotifications($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereAllowTelegramNotifications($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereTelegramChatId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereTelegramUsername($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User withRole($role)
 * @method static \Illuminate\Database\Query\Builder|\App\User withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\User withoutTrashed()
 */
	class User extends \Eloquent {}
}

namespace App{
/**
 * App\UuidModel
 *
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\UuidModel onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Query\Builder|\App\UuidModel withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\UuidModel withoutTrashed()
 */
	class UuidModel extends \Eloquent {}
}


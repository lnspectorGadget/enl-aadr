@extends('layouts.home')

@section('content')
<div class="jumbotron">
    <div class="container">
        <h1>Hallo recruit!</h1>
        <p>Er is iets mis met de wereld. Voel jij dat ook? Het is tijd om de strijd aan te gaan. De strijd tegen The Resistance om de Exotic Matter, die wij hard nodig hebben de mensheid te verbeteren.</p>
        <p><a class="btn btn-enlightened btn-lg" href="#" role="button">Doe mee &raquo;</a></p>
    </div>
</div>

<div class="container">
    <!-- Example row of columns -->
    <div class="row">
        <div class="col-md-6">
            <h2>Wat is Ingress?</h2>
            <p>Ingress is een zogenoemde augmented reality spel wat gespeeld wordt op je smartphone of tablet met GPS in de echte wereld. </p>
            <p>Het spel kent twee teams: The Enlightened (Groen) en The Resistance (Blauw). De Enlightened wil de krachten van de XM gebruiken om de mensheid verder te ontwikkelen en naar een hoger niveau te tillen. De Resistance zijn bang dat de XM mensen hersenspoelt en indoctrineert en probeert zich tegen de XM te verzetten.</p>
        </div>
        <div class="col-md-6">
            <h2>Wie zijn wij?</h2>
            <p>Wij zijn een gezellige groep mensen van verschillende leeftijden, met verschillende achtergronden en hobbies. Maar één ding hebben wij allemaal gemeen, wij zijn Enlightened! Groen is onze kleur en die dragen wij dan ook met trots. Sluit je bij ons aan en speel samen met het leukste en gezelligste team van Alphen aan den Rijn!</p>
        </div>
    </div>

    <hr>

    <footer>
        <p>&copy; {{ date('Y') }} The Enlightened, Alphen aan den Rijn.</p>
    </footer>
</div> <!-- /container -->
@endsection

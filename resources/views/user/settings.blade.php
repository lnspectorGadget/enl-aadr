@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Mijn account</div>
                    <div class="panel-body">
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif
                        <form class="form-horizontal" method="POST" action="{{ route('user.settings.update', ['user' => $user->id]) }}">
                            {{ csrf_field() }}
                            {{ method_field('PATCH') }}
                            <div class="form-group{{ $errors->has('user.name') ? ' has-error' : '' }}">
                                <label for="user-name" class="col-md-4 control-label">Naam</label>

                                <div class="col-md-6">
                                    <input id="user-name" type="text" class="form-control" name="user[name]" value="{{ $user->name }}" required autofocus>
                                    @if ($errors->has('user.name'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('user.name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('user.password') ? ' has-error' : '' }}">
                                <label for="user-password" class="col-md-4 control-label">Wachtwoord</label>

                                <div class="col-md-6">
                                    <span class="label label-primary">Laat leeg om je wachtwoord niet te wijzigen.</span>
                                    <input id="user-password" type="password" class="form-control" name="user[password]" value="">
                                    @if ($errors->has('user.password'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('user.password') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('user.password_confirmation') ? ' has-error' : '' }}">
                                <label for="user-password_confirm" class="col-md-4 control-label">Bevestig wachtwoord</label>

                                <div class="col-md-6">
                                    <input id="user-password_confirm" type="password" class="form-control" name="user[password_confirmation]" value="">
                                    @if ($errors->has('user.password_confirmation'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('user.password_confirmation') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('user.allow_mail_notifications') ? ' has-error' : '' }}">
                                <div class="col-md-6 col-md-push-4">
                                    <input id="user-allow_mail_notifications" type="checkbox" name="user[allow_mail_notifications]" value="1" @if ($user->allow_mail_notifications) checked @endif>
                                    <label for="user-allow_mail_notifications" class="control-label">Ontvang updates via mail?</label>
                                    @if ($errors->has('user.allow_mail_notifications'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('user.allow_mail_notifications') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('user.allow_telegram_notifications') ? ' has-error' : '' }}">
                                <div class="col-md-6 col-md-push-4">
                                    <input id="user-allow_telegram_notifications" type="checkbox" name="user[allow_telegram_notifications]" value="1" @if ($user->allow_telegram_notifications) checked @endif>
                                    <label for="user-allow_telegram_notifications" class="control-label">Ontvang updates via Telegram?</label>
                                    @if ($errors->has('user.allow_telegram_notifications'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('user.allow_telegram_notifications') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div @if ($user->allow_telegram_notifications !== 1) style="display: none" @endif id="telegram_username" class="form-group{{ $errors->has('user.telegram_username') ? ' has-error' : '' }}">
                                <label for="user-telegram_username" class="col-md-4 control-label">Telegram gebruikersnaam</label>

                                <div class="col-md-6">
                                    <input id="user-telegram_username" type="text" class="form-control" name="user[telegram_username]" value="{{ $user->telegram_username }}">
                                    <div class="alert alert-info">Nadat je Telegram gebruikersnaam is opgeslagen, open Telegram en start een nieuwe chat met <strong>@enl_ingress_bank_bot</strong>. Druk op `Begin` en klaar!</div>
                                    @if ($errors->has('user.telegram_username'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('user.telegram_username') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>



                            <hr>

                            <div class="form-group{{ $errors->has('player.name') ? ' has-error' : '' }}">
                                <label for="player-name" class="col-md-4 control-label">IGN</label>

                                <div class="col-md-6">
                                    <input id="player-name" type="text" class="form-control" name="player[name]" value="{{ $user->player->name }}" required>
                                    @if ($errors->has('player.name'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('player.name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-8 col-md-offset-4">
                                    <button type="submit" class="btn btn-success">
                                        Opslaan
                                    </button>
                                    <a class="btn btn-default" href="{{ route('users.index') }}">
                                        Annuleren
                                    </a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        $('#user-allow_telegram_notifications').on('click', function() {
            let checked = $(this).prop('checked');
            if (checked) {
                $('#telegram_username').show();
            } else {
                $('#telegram_username').hide();
            }
        });
    </script>
@endsection

@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Registreer</div>
                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('users.register-store', ['registerToken' => $registerToken]) }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('user.name') ? ' has-error' : '' }}">
                            <label for="user-name" class="col-md-4 control-label">Naam</label>

                            <div class="col-md-6">
                                <input id="user-name" type="text" class="form-control" name="user[name]" value="{{ old('user.name') }}" required autofocus>
                                @if ($errors->has('user.name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('user.name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('user.email') ? ' has-error' : '' }}">
                            <label for="user-email" class="col-md-4 control-label">E-mailadres</label>

                            <div class="col-md-6">
                                <input id="user-email" type="text" class="form-control" name="user[email]" value="{{ old('user.email') }}" required>
                                @if ($errors->has('user.email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('user.email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('user.password') ? ' has-error' : '' }}">
                            <label for="user-password" class="col-md-4 control-label">Wachtwoord</label>
                            <div class="col-md-6">
                                <input id="user-password" type="password" class="form-control" name="user[password]" value="{{ old('user.password') }}" required>
                                @if ($errors->has('user.password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('user.password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('user.password_confirmation') ? ' has-error' : '' }}">
                            <label for="user-password_confirm" class="col-md-4 control-label">Bevestig wachtwoord</label>

                            <div class="col-md-6">
                                <input id="user-password_confirmation" type="password" class="form-control" name="user[password_confirmation]" value="{{ old('user.password_confirmation') }}" required>
                                @if ($errors->has('user.password_confirm'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('user.password_confirmation') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('user.allow_mail_notifications') ? ' has-error' : '' }}">
                            <div class="col-md-6 col-md-push-4">
                                <input id="user-allow_mail_notifications" type="checkbox" name="user[allow_mail_notifications]" value="1" @if(old('user.allow_mail_notifications') === '1') checked @endif>
                                <label for="user-allow_mail_notifications" class="control-label">Ontvang updates via e-mail?</label>
                                @if ($errors->has('user.allow_mail_notifications'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('user.allow_mail_notifications') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('user.allow_telegram_notifications') ? ' has-error' : '' }}">
                            <div class="col-md-6 col-md-push-4">
                                <input id="user-allow_telegram_notifications" type="checkbox" name="user[allow_telegram_notifications]" value="1" @if(old('user.allow_telegram_notifications') === '1') checked @endif>
                                <label for="user-allow_telegram_notifications" class="control-label">Ontvang updates via Telegram?</label>
                                @if ($errors->has('user.allow_telegram_notifications'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('user.allow_telegram_notifications') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div @if(old('user.allow_telegram_notifications') !== '1')style="display: none"@endif id="telegram_username" class="form-group{{ $errors->has('user.telegram_username') ? ' has-error' : '' }}">
                            <label for="user-telegram_username" class="col-md-4 control-label">Telegram gebruikersnaam</label>

                            <div class="col-md-6">
                                <input id="user-telegram_username" type="text" class="form-control" name="user[telegram_username]" value="{{ old('user.telegram_username') }}">
                                @if ($errors->has('user.telegram_username'))
                                    <span class="help-block">
                                            <strong>{{ $errors->first('user.telegram_username') }}</strong>
                                        </span>
                                @endif
                            </div>
                        </div>

                        <hr>

                        <div class="form-group{{ $errors->has('player.name') ? ' has-error' : '' }}">
                            <label for="player-name" class="col-md-4 control-label">IGN</label>

                            <div class="col-md-6">
                                <input id="player-name" type="text" class="form-control" name="player[name]" value="{{ old('player.name') }}" required>
                                @if ($errors->has('player.name'))
                                    <span class="help-block">
                                            <strong>{{ $errors->first('player.name') }}</strong>
                                        </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Registreren
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
    <script>
        $('#user-allow_telegram_notifications').on('click', function() {
            let checked = $(this).prop('checked');
            if (checked) {
                $('#telegram_username').show();
            } else {
                $('#telegram_username').hide();
            }
        });
    </script>
@endsection

@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h2>Bank voorwerpen</h2>
            @foreach ($capsules as $capsule)
            <h3>{{ $capsule->code }} @role('administrator')(Houder: {{ $capsule->holder->name }})@endrole</h3>
            <table id="capsule-{{ $capsule->code }}" class="table table-responsive table-bordered">
                <thead>
                    <tr>
                        <th><input id="checkAll-{{ $capsule->code }}" type="checkbox" /></th>
                        <th>Voorwerp</th>
                        <th>Aantal</th>
                        <th>Eigenaar</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($capsule->items as $item)
                        <tr>
                            <td><input class="check" type="checkbox" value="{{$item->id}}" /></td>
                            <td>{{ $item->item->name }}</td>
                            <td>{{ $item->amount }}</td>
                            <td>{{ $item->owner->name }}</td>
                            <td>
                                <a class="btn btn-sm btn-default" href="{{ route('bankitems.edit', ['bankItem' => $item->id]) }}">Wijzig</a>
                                <a class="btn btn-sm btn-danger delete" data-action="{{ route('bankitems.delete', ['bankItem' => $item->id]) }}" href="#">Verwijder</a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="5">
                            <a class="btn btn-success" href="{{ route('bankitems.create', ['capsule' => $capsule->id]) }}">
                                Creëer bank voorwerp
                            </a>
                            <a id="register-duplication-{{ $capsule->code }}" class="btn btn-success"
                               data-dropdown="{{ $capsule->items()->with('item')->groupBy('item_id')->distinct()->get()->toJson() }}"
                               data-action="{{ route('capsules.store-duplication', ['capsule' => $capsule->id]) }}">
                                Registeer duplicatie
                            </a>
                            <button disabled id="mass-delete-{{ $capsule->code }}" class="btn btn-danger pull-right">
                                Verwijder selectie
                            </button>
                        </td>
                    </tr>
                </tfoot>
            </table>
            @endforeach
        </div>
    </div>
</div>
@endsection

@section('modals')
    <div class="modal fade" id="delete-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form id="delete-modal-form" class="form-horizontal" method="POST" action="">
                    {{ csrf_field() }}
                    {{ method_field('DELETE') }}
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Bevestigen</h4>
                    </div>
                    <div class="modal-body">
                        Weet je zeker dat je dit bank voorwerp wil verwijderen?
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Annuleren</button>
                        <input type="submit" class="btn btn-danger" value="Verwijder bank voorwerp">
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal fade" id="duplication-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form id="duplication-modal-form" class="form-horizontal" method="POST" action="">
                    {{ csrf_field() }}
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Registeer duplicatie</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="item_id" class="col-md-4 control-label">Item</label>

                            <div class="col-md-6">
                                <select id="item_id" class="form-control" name="item_id" size="1">

                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="amount" class="col-md-4 control-label">Amount</label>

                            <div class="col-md-6">
                                <input id="amount" type="number" class="form-control" name="amount" value="" required autofocus>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        <input type="submit" class="btn btn-success" value="Register">
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal fade" id="mass-delete-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form id="mass-delete-modal-form" class="form-horizontal" method="POST" action="{{ route('bankitems.mass-delete') }}">
                    {{ csrf_field() }}
                    {{ method_field('DELETE') }}
                    <div id="input-placeholder"></div>
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Bevestigen</h4>
                    </div>
                    <div class="modal-body">
                        Weet je zeker dat je deze bank voorwerpen wil verwijderen?
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        <input type="submit" class="btn btn-danger" value="Delete BankItems">
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script src="{{ asset('js/datatable.js') }}"></script>
    <script>
        $('.delete').on('click', function (e) {
            e.preventDefault();
            $('#delete-modal-form').attr('action', $(this).data('action'));
            $('#delete-modal').modal({"backdrop": "static"});
        });
    </script>
    <script>
        @foreach ($capsules as $capsule)
        $('#mass-delete-{{ $capsule->code }}').on('click', function (e) {
            e.preventDefault();
            $('#capsule-{{ $capsule->code }} .check:checked').each(function () {
                $('#input-placeholder').append('<input type="hidden" name="ids[]" value="' + $(this).val() + '">');
            });
            $('#mass-delete-modal').modal({"backdrop": "static"});
        }).prop('disabled', true);

        $('#register-duplication-{{ $capsule->code }}').on('click', function (e) {
            e.preventDefault();
            $('#duplication-modal-form').attr('action', $(this).data('action'));
            let dropdown = $(this).data('dropdown');
            $.each(dropdown, function(index, data) {
                $('#item_id').append('<option value="' + data.item.id + '">' + data.item.name + '</option>');
            });

            $('#duplication-modal').modal({"backdrop": "static"});
        }).prop('disabled', true);

        $('#capsule-{{ $capsule->code }} .check').on('click', function () {
            $('#mass-delete-{{ $capsule->code }}').prop('disabled', true);
            $('#capsule-{{ $capsule->code }} .check:checked').each(function () {
                $('#mass-delete-{{ $capsule->code }}').prop('disabled', false);
            });
        });

        $('#checkAll-{{ $capsule->code }}').on('click', function() {
            let checked = $(this).prop('checked');
            $('#mass-delete-{{ $capsule->code }}').prop('disabled', !checked);
            $('#capsule-{{ $capsule->code }} .check').each(function () {
                $(this).prop('checked', checked);
            });
        });
        $(document).ready(function() {
            $('#capsule-{{ $capsule->code }}').DataTable({
                "columnDefs": [
                    { "orderable": false, "targets": [0,4] },
                    { "width": "15px", "targets": 0 },
                    { "width": "140px", "targets": 4 }
                ],
                "order": [[ 1, 'asc' ]],
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Dutch.json"
                }
            });
        } );
        @endforeach
    </script>
@endsection
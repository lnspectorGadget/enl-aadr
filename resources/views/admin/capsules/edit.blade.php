@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Wijzig capsule</div>
                    <div class="panel-body">
                        <form class="form-horizontal" method="POST" action="{{ route('capsules.update', ['capsule' => $capsule->id]) }}">
                            {{ csrf_field() }}
                            {{ method_field('PATCH') }}
                            <div class="form-group{{ $errors->has('code') ? ' has-error' : '' }}">
                                <label for="email" class="col-md-4 control-label">Code</label>

                                <div class="col-md-6">
                                    <input id="code" type="text" class="form-control" name="code" value="{{ $capsule->code }}" required autofocus>
                                    @if ($errors->has('code'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('code') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('player_owner_id') ? ' has-error' : '' }}">
                                <label for="player_owner_id" class="col-md-4 control-label">Eigenaar</label>

                                <div class="col-md-6">
                                    <select id="player_owner_id" class="form-control" name="player_owner_id" size="1">
                                        @foreach($players as $player)
                                            <option value="{{ $player->id }}" @if($player->id === $capsule->owner_player_id) selected="selected" @endif>{{ $player->name }}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('player_owner_id'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('player_owner_id') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            @role('administrator')
                            <div class="form-group{{ $errors->has('player_holder_id') ? ' has-error' : '' }}">
                                <label for="player_holder_id" class="col-md-4 control-label">Houder</label>

                                <div class="col-md-6">
                                    <select id="player_holder_id" class="form-control" name="player_holder_id" size="1">
                                        @foreach($players as $player)
                                            <option value="{{ $player->id }}" @if($player->id === $capsule->holder_player_id) selected="selected" @endif>{{ $player->name }}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('player_holder_id'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('player_holder_id') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            @endrole

                            <div class="form-group">
                                <div class="col-md-8 col-md-offset-4">
                                    <button type="submit" class="btn btn-success">
                                        Opslaan
                                    </button>
                                    <a class="btn btn-default" href="{{ route('capsules.index') }}">
                                        Annuleren
                                    </a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
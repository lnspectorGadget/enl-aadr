@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h2>Capsules</h2>
            <table id="capsules" class="table table-responsive table-bordered">
                <thead>
                    <tr>
                        <th><input id="checkAll" type="checkbox" /></th>
                        <th>Code</th>
                        <th>Eigenaar</th>
                        @role('administrator')<th>Houder</th>@endrole
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($capsules as $capsule)
                        <tr>
                            <td><input class="check" type="checkbox" value="{{$capsule->id}}" /></td>
                            <td>{{ $capsule->code }}</td>
                            <td>{{ $capsule->owner->name }}</td>
                            @role('administrator')<td>{{ $capsule->holder->name }}</td>@endrole
                            <td>
                                <a class="btn btn-sm btn-default" href="{{ route('capsules.edit', ['capsule' => $capsule->id]) }}">Wijzig</a>
                                <button class="btn btn-sm btn-danger delete" data-action="{{ route('capsules.delete', ['capsule' => $capsule->id]) }}">Verwijder</button>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        @role('administrator')
                        <td colspan="5">
                        @endrole
                        @role('banker')
                        <td colspan="4">
                        @endrole
                            <a class="btn btn-success" href="{{ route('capsules.create') }}">
                                Creëer capsule
                            </a>
                            <button disabled id="mass-delete" class="btn btn-danger pull-right">
                                Verwijder selectie
                            </button>
                        </td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>
@endsection

@section('modals')
<div class="modal fade" id="delete-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form id="delete-modal-form" class="form-horizontal" method="POST" action="">
                {{ csrf_field() }}
                {{ method_field('DELETE') }}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Bevestigen</h4>
                </div>
                <div class="modal-body">
                    Weet je zeker dat je deze capsules wil verwijderen?
                    Alle voorwerpen die in deze capsules zitten worden hierdoor ook verwijderd!
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Annuleren</button>
                    <input type="submit" class="btn btn-danger" value="Verwijder capsule">
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="mass-delete-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form id="mass-delete-modal-form" class="form-horizontal" method="POST" action="{{ route('capsules.mass-delete') }}">
                {{ csrf_field() }}
                {{ method_field('DELETE') }}
                <div id="input-placeholder"></div>
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Bevestigen</h4>
                </div>
                <div class="modal-body">
                    Weet je zeker dat je deze capsules wil verwijderen?
                    Alle voorwerpen die in deze capsules zitten worden hierdoor ook verwijderd!
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Annuleren</button>
                    <input type="submit" class="btn btn-danger" value="Verwijder capsules">
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
@section('scripts')<script src="{{ asset('js/datatable.js') }}"></script>
<script>
    $('.delete').on('click', function (e) {
        e.preventDefault();
        $('#delete-modal-form').attr('action', $(this).data('action'));
        $('#delete-modal').modal({"backdrop": "static"});
    });
    $('#mass-delete').on('click', function (e) {
        e.preventDefault();
        $('.check:checked').each(function () {
            $('#input-placeholder').append('<input type="hidden" name="ids[]" value="' + $(this).val() + '">');
        });
        $('#mass-delete-modal').modal({"backdrop": "static"});
    }).prop('disabled', true);
    $('.check').on('click', function () {
        $('#mass-delete').prop('disabled', true);
        $('.check:checked').each(function () {
            $('#mass-delete').prop('disabled', false);
        });
    });
    $('#checkAll').on('click', function() {
        let checked = $(this).prop('checked');
        $('#mass-delete').prop('disabled', !checked);
        $('.check').each(function () {
            $(this).prop('checked', checked);
        });
    });
</script>
@role('administrator')
<script>
    $(document).ready(function() {
        $('#capsules').DataTable({
            "columnDefs": [
                { "orderable": false, "targets": [0,4] },
                { "width": "15px", "targets": 0 },
                { "width": "140px", "targets": 4 }
            ],
            "order": [[ 1, 'asc' ]],
            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Dutch.json"
            }
        });
    } );
</script>
@endrole
@role('banker')
<script>
    $(document).ready(function() {
        $('#capsules').DataTable({
            "columnDefs": [
                { "orderable": false, "targets": [0,3] },
                { "width": "15px", "targets": 0 },
                { "width": "140px", "targets": 3 }
            ],
            "order": [[ 1, 'asc' ]],
            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Dutch.json"
            }
        });
    } );
</script>
@endrole
@endsection
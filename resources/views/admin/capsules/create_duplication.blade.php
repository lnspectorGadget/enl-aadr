@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Create Capsule</div>
                    <div class="panel-body">
                        <form class="form-horizontal" method="POST" action="{{ route('capsules.store-duplication', ['capsule' => $capsule->id]) }}">
                            {{ csrf_field() }}
                            <div class="form-group{{ $errors->has('item_id') ? ' has-error' : '' }}">
                                <label for="item_id" class="col-md-4 control-label">eeeeeeeee</label>

                                <div class="col-md-6">
                                    <select id="item_id" class="form-control" name="item_id" size="1">
                                        @foreach($items as $item)
                                            <option value="{{ $item->id }}" @if($item->id === old('item_id')) selected="selected" @endif>{{ $item->name }}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('item_id'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('item_id') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('amount') ? ' has-error' : '' }}">
                                <label for="amount" class="col-md-4 control-label">Amount</label>

                                <div class="col-md-6">
                                    <input id="amount" type="number" class="form-control" name="amount" value="{{ old('amount') }}" required autofocus>
                                    @if ($errors->has('amount'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('amount') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-8 col-md-offset-4">
                                    <button type="submit" class="btn btn-success">
                                        Save
                                    </button>
                                    <a class="btn btn-default" href="{{ route('capsules.index') }}">
                                        Cancel
                                    </a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h2>Voorwerpen</h2>
            <table id="capsules" class="table table-responsive table-bordered">
                <thead>
                    <tr>
                        <th><input id="checkAll" type="checkbox" /></th>
                        <th>Naam</th>
                        <th>Zeldzaamheid</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($items as $item)
                        <tr>
                            <td><input class="check" type="checkbox" value="{{$item->id}}" /></td>
                            <td>{{ $item->name }}</td>
                            <td>{{ $item->getRarity() }}</td>
                            <td>
                                <span class="pull-right">
                                    <a class="btn btn-sm btn-default" href="{{ route('items.edit', ['item' => $item->id]) }}">Wijzig</a>
                                    <a class="btn btn-sm btn-danger delete" data-action="{{ route('items.delete', ['item' => $item->id]) }}" href="#">Verwijder</a>
                                </span>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="4">
                            <a class="btn btn-success" href="{{ route('items.create') }}">
                                Creëer voorwerp
                            </a>
                            <button disabled id="mass-delete" class="btn btn-danger pull-right">
                                Verwijder selectie
                            </button>
                        </td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>
@endsection

@section('modals')
    <div class="modal fade" id="delete-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form id="delete-modal-form" class="form-horizontal" method="POST" action="">
                    {{ csrf_field() }}
                    {{ method_field('DELETE') }}
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Bevestiging</h4>
                    </div>
                    <div class="modal-body">
                        Weet je zeker dat je dit voorwerp wil verwijderen?
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        <input type="submit" class="btn btn-danger" value="Delete Item">
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal fade" id="mass-delete-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form id="mass-delete-modal-form" class="form-horizontal" method="POST" action="{{ route('items.mass-delete') }}">
                    {{ csrf_field() }}
                    {{ method_field('DELETE') }}
                    <div id="input-placeholder"></div>
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Bevestiging</h4>
                    </div>
                    <div class="modal-body">
                        Weet je zeker dat je deze voorwerpen wil verwijderen?
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Annuleren</button>
                        <input type="submit" class="btn btn-danger" value="Verwijder voorwerpen">
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script src="{{ asset('js/datatable.js') }}"></script>
    <script>
        $('.delete').on('click', function (e) {
            e.preventDefault();
            $('#delete-modal-form').attr('action', $(this).data('action'));
            $('#delete-modal').modal({"backdrop": "static"});
        });

        $('#mass-delete').on('click', function (e) {
            e.preventDefault();
            $('.check:checked').each(function () {
                $('#input-placeholder').append('<input type="hidden" name="ids[]" value="' + $(this).val() + '">');
            });
            $('#mass-delete-modal').modal({"backdrop": "static"});
        }).prop('disabled', true);

        $('.check').on('click', function () {
            $('#mass-delete').prop('disabled', true);
            $('.check:checked').each(function () {
                $('#mass-delete').prop('disabled', false);
            });
        });

        $('#checkAll').on('click', function() {
            let checked = $(this).prop('checked');
            $('#mass-delete').prop('disabled', !checked);
            $('.check').each(function () {
                $(this).prop('checked', checked);
            });
        });

        $(document).ready(function() {
            $('#capsules').DataTable({
                "columnDefs": [
                    { "orderable": false, "targets": [0,3] },
                    { "width": "15px", "targets": 0 },
                    { "width": "140px", "targets": 3 }
                ],
                "order": [[ 1, 'asc' ]],
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Dutch.json"
                }
            });
        } );
    </script>
@endsection
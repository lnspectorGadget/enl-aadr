@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Wijzig voorwerp</div>
                    <div class="panel-body">
                        <form class="form-horizontal" method="POST" action="{{ route('items.update', ['item' => $item->id]) }}">
                            {{ csrf_field() }}
                            {{ method_field('PATCH') }}
                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <label for="item-name" class="col-md-4 control-label">Naam</label>

                                <div class="col-md-6">
                                    <input id="item-name" type="text" class="form-control" name="name" value="{{ $item->name }}" required autofocus>
                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('rarity') ? ' has-error' : '' }}">
                                <label for="item-rarity" class="col-md-4 control-label">Zeldzaamheid</label>

                                <div class="col-md-6">
                                    <select id="item-rarity" class="form-control" name="rarity" size="1">
                                        @foreach($rarities as $key => $rarity)
                                            <option value="{{ $key }}" @if($key === $item->rarity) selected="selected" @endif>{{ $rarity }}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('rarity'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('rarity') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-8 col-md-offset-4">
                                    <button type="submit" class="btn btn-success">
                                        Opslaan
                                    </button>
                                    <a class="btn btn-default" href="{{ route('items.index') }}">
                                        Annuleren
                                    </a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
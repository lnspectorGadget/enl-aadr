@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h2>Gebruikers</h2>
            <table id="users" class="table table-responsive table-bordered">
                <thead>
                    <tr>
                        @role('administrator')
                            <th><input id="checkAll" type="checkbox"></th>
                        @endrole
                        <th>Naam</th>
                        <th>E-mailadres</th>
                        <th><i class="fas fa-envelope"></i></th>
                        <th><i class="fab fa-telegram"></i></th>
                        <th>Telegram gebruikersnaam</th>
                        <th>Speler</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($users as $user)
                        <tr>
                            @role('administrator')
                                <td><input class="check" type="checkbox" value="{{$user->id}}" /></td>
                            @endrole
                            <td>{{ $user->name }}</td>
                            <td>{{ $user->email }}</td>
                            <td>{{ $user->allow_mail_notifications ? 'Yes' : 'No' }}</td>
                            <td>{{ $user->allow_telegram_notifications ? 'Yes' : 'No' }}</td>
                            <td>{{ $user->telegram_username }}</td>
                            <td>{{ $user->player->name }}</td>
                            <td>
                                <a class="btn btn-sm btn-default" href="{{ route('users.edit', ['user' => $user->id]) }}">Wijzig</a>
                                <a class="btn btn-sm btn-danger delete" data-action="{{ route('users.delete', ['user' => $user->id]) }}" href="#">Verwijder</a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="8">
                            <a class="btn btn-success" href="{{ route('users.create') }}">
                                Creëer gebruiker
                            </a>
                            <button disabled id="mass-delete" class="btn btn-danger pull-right">
                                Verwijder selectie
                            </button>
                        </td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>
@endsection

@section('modals')
    <div class="modal fade" id="delete-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form id="delete-modal-form" class="form-horizontal" method="POST" action="">
                    {{ csrf_field() }}
                    {{ method_field('DELETE') }}
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Bevestiging</h4>
                    </div>
                    <div class="modal-body">
                        Weet je zeker dat je deze gebruiker wil verwijderen?
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Annuleren</button>
                        <input type="submit" class="btn btn-danger" value="Verwijder gebruiker">
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal fade" id="mass-delete-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form id="mass-delete-modal-form" class="form-horizontal" method="POST" action="{{ route('users.mass-delete') }}">
                    {{ csrf_field() }}
                    {{ method_field('DELETE') }}
                    <div id="input-placeholder"></div>
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Bevestiging</h4>
                    </div>
                    <div class="modal-body">
                        Weet je zeker dat je deze gebruikers wil verwijderen?
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Annuleren</button>
                        <input type="submit" class="btn btn-danger" value="Verwijder gebruikers">
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script src="{{ asset('js/datatable.js') }}"></script>
    <script>
        $('.delete').on('click', function (e) {
            e.preventDefault();
            $('#delete-modal-form').attr('action', $(this).data('action'));
            $('#delete-modal').modal({"backdrop": "static"});
        });

        $('#mass-delete').on('click', function (e) {
            e.preventDefault();
            $('#users .check:checked').each(function () {
                $('#input-placeholder').append('<input type="hidden" name="ids[]" value="' + $(this).val() + '">');
            });
            $('#mass-delete-modal').modal({"backdrop": "static"});
        }).prop('disabled', true);

        $('#users .check').on('click', function () {
            $('#mass-delete').prop('disabled', true);
            $('#users .check:checked').each(function () {
                $('#mass-delete').prop('disabled', false);
            });
        });

        $('#checkAll').on('click', function() {
            let checked = $(this).prop('checked');
            $('#mass-delete').prop('disabled', !checked);
            $('#users .check').each(function () {
                $(this).prop('checked', checked);
            });
        });

        $(document).ready(function() {
            $('#users').DataTable({
                "columnDefs": [
                    { "orderable": false, "targets": [0,7] },
                    { "width": "15px", "targets": 0 },
                    { "width": "140px", "targets": 7 }
                ],
                "order": [[ 1, 'asc' ]],
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Dutch.json"
                }
            });
        } );
    </script>
@endsection
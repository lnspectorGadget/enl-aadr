@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>Jouw voorwerpen</h2>
                <table id="bankItems" class="table table-responsive table-bordered">
                    <thead>
                        <tr>
                            <th class="col-md-2">Capsule</th>
                            <th class="col-md-4">Houder</th>
                            <th class="col-md-2">Aantal</th>
                            <th class="col-md-2">Voorwerp</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($bankItems as $bankItem)
                            <tr>
                                <td>{{ $bankItem->capsule->code }}</td>
                                <td>{{ $bankItem->capsule->holder->name }}</td>
                                <td>{{ $bankItem->amount }}</td>
                                <td>{{ $bankItem->item->name }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <th class="col-md-2">Capsule</th>
                            <th class="col-md-4">Houder</th>
                            <th class="col-md-2">Aantal</th>
                            <th class="col-md-2">Voorwerp</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script src="{{ asset('js/datatable.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('#bankItems').DataTable({
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Dutch.json"
                }
            });
        });
    </script>
@endsection